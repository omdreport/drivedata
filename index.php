<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="icon" type="image/png" href="favicon-omd.png" sizes="18x18 32x32">
  <title>OMD</title>
  <link rel="stylesheet" type="text/css" href="public/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="public/css/styleLogin.css">
	<!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="public/css/styleLogin.css">
  <link rel="stylesheet" href="./styles.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
  <body>
  <div class="container" id="app-loguin">
    <div class="d-flex justify-content-center h-100">
      <div class="card-body" style="margin-left: 35%; margin-top: 26%;">
        <div class="alert alert-danger error-loguin" id="error-loguin" role="alert" v-if="error">{{ message }}</div>
        <form action="post" v-on:submit.prevent="validateUsers" class="form-box animated fadeInUp">
          <h3  class="form-title">Ingresar</h3>
          <div class="input-group form-group">
            <input type="text" v-model="dataUser.userName" name="name" id="name" placeholder="usuario">
          </div>
          <div class="input-group form-group">
            <input v-model="dataUser.userPass" type="password" name="password" id="pasword" placeholder="clave">
          </div>
          <div class="form-group">
            <input class="btn btn-primary login_btn" type="submit" name="accion" id="loguin" value="Ingresar">
          </div>
        </form>
      </div>
    </div>
  </div> 
    <!--<script src="public/js/authen.js"></script>-->
    <script src="public/vue/vue.js"></script>
    <script src="public/js/authentic.js"></script>
  </body>
</html>