<!-- The Modal -->
<div 
  class="modal" 
  id="myModal" 
  v-if="modalUser">
  <div 
    class="modal-dialog modal-dialog-custom">
    <div 
      class="modal-content modal-content-cumston">
    
      <div 
        class="modal-header">
        <h4 
          class="modal-title">Editar Usuario</h4>
        <button 
          type="button" 
          class="close" 
          data-dismiss="modal">&times;</button>
      </div>
      
      <div class="modal-body">
        <blockquote class="blockquote mb-0">
          <form @submit.prevent="editUserForm">
            <fieldset>
              <div 
                class="form-group row row-form" 
                hidden>
                <div class="col-md-8">
                  <input 
                    id="id" 
                    v-model="dataEditUser.id" 
                    type="text" 
                    placeholder="id" 
                    class="form-control">
                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-6 form-font">
                  <label><strong> Cedula:</strong></label>
                  <input 
                    v-model="dataEditUser.num_documento" 
                    type="text" 
                    placeholder="Cedula" 
                    name="Cedula" 
                    v-validate="'required'" 
                    class="form-control" 
                    v-on:change="consultDocument($event)">
                  <span class="error" v-if="errors.has('Cedula')">{{errors.first('Cedula')}}</span>
                </div>

                <div class="col-md-6 form-font" >
                  <label>Nombres:</label>
                  <input 
                    v-model="dataEditUser.nom_usuario" 
                    type="text" 
                    placeholder="Nombres" 
                    name="nombres"
                    v-validate="'required'" 
                    class="form-control">
                  <span class="error" v-if="errors.has('nombres')">{{errors.first('nombres')}}</span>
                </div>
              </div>
              
              <div class="form-group row row-form">
                <div class="col-md-6 form-font">
                  <label>Apellidos:</label>
                  <input 
                    name="Apellidos" 
                    v-model="dataEditUser.ape_usuario" 
                    type="text" 
                    placeholder="Apellidos" 
                    v-validate="'required'" 
                    class="form-control">
                  <span class="error" v-if="errors.has('Apellidos')">{{errors.first('Apellidos')}}</span>
                </div>

                <div class="col-md-6 form-font">
                  <label>Correo:</label>
                  <input 
                    v-model="dataEditUser.usuario" 
                    type="text" 
                    placeholder="Usuario" 
                    name="Usuario"
                    v-validate="'required'" 
                    class="form-control">
                  <span class="error" v-if="errors.has('Usuario')">{{errors.first('Usuario')}}</span>

                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-6 form-font">
                  <label><strong>Contraseña:</strong></label>
                  <input 
                    name="Contraseña" 
                    v-model="dataEditUser.password" 
                    type="password" 
                    placeholder="Contraseña" 
                    v-validate="'required'" 
                    class="form-control">
                  <span class="error" v-if="errors.has('Contraseña')">{{errors.first('Contraseña')}}</span>
                </div>
                <div class="col-md-6 form-font">
                  <label><strong>Cargo:</strong></label>
                  <select 
                    class="browser-default custom-select form-control" 
                    name="Perfil" 
                    v-model="dataEditUser.id_perfil" 
                    v-validate="'required'">
                    <option 
                      selected v-for="(option, index) in dataPerfil" 
                      v-bind:value="option.id">{{ option.nom_perfil }}</option>
                    }
                  </select>
                  <span class="error" v-if="errors.has('Perfil')">{{errors.first('Perfil')}}</span>
                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-6 form-font">
                    <label><strong>Perfil:</strong></label> 
                     {{ dataEditUser.id_roles }}
                    <select 
                      class="browser-default custom-select form-control" 
                      v-model="dataEditUser.id_role" 
                      name="Roll" 
                      v-validate="'required'">
                      <option 
                        selected 
                        v-for="(option, index) in dataRoles" 
                        v-bind:value="option.id">{{ option.nom_role }}</option>
                    </select>
                    <span class="error" v-if="errors.has('Roll')">{{errors.first('Roll')}}</span>
                </div>

                <div class="col-md-6 form-font">
                  <label style="padding-right: 22px;margin-top: 29px;"><strong>Seleccionar Tareas:</strong></label>
                  <input 
                    type="checkbox" 
                    name="tarea" 
                    v-model="checked" 
                    v-validate="'required'" 
                    @click.self="getTaskSelect($event,checked)" 
                    data-toggle="modal" 
                    data-target="#myModalUser1">
                  <span class="error" v-if="errors.has('tarea')">{{errors.first('tarea')}}</span>
                </div>
              </div>

               <div class="form-group row row-form"> 
                <div class="col-md-12 form-font">
                  <fieldset style="border-radius: 9px;" class="form-font">
                    <legend><strong>Seleccionar Clientes:</strong></legend>
                  <div class="row">
                    <div 
                      class="form-check form-check-inline col-md-2" 
                      v-for="(option,index) in dataClients" 
                      style=" font-size: 10px;margin-left: 39px;">
                      <input 
                        class="form-check-input" 
                        type="checkbox" 
                        id="inlineCheckbox1" 
                        :checked="clientsCheckAll[index]"
                        value="option1"
                        @click.self="getClientsSelect($event,dataClients[index])">
                      <label class="form-check-label" for="inlineCheckbox1"> {{ dataClients[index].nom_cliente }} </label>
                    </div>
                  </div>
                  </fieldset>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12 text-center">
                  <button type="submit" class="btn btn-primary btn-lg btn-form btn-add-user">Guardar</button>
                </div>
              </div>
            </fieldset>
            
            
          </form>
        </blockquote>
      </div>
      
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->
      
    </div>
  </div>
</div>