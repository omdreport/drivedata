<div 
	class="modal"  
	id="myModalUser1" 
	v-if="listTask">
  <div 
  	class="modal-dialog modal-dialog-custom">
    <div 
    	class="modal-content modal-content-cumston">
    
      <div class="modal-header">
        <h4 class="modal-title">Seleccionar Tareas </h4>
        <button 
        	type="button" 
        	class="btn btn-primary btn-add-btn" 
        	@click="addCheckTask()" 
        	style="position:relative;margin-left:6%;margin-top:-1px;">Asignar Tareas
        </button>
        <button 
        	type="button" 
        	class="close" 
        	data-dismiss="modal">&times;
        </button>
      </div>
      
      <div class="modal-body">
				<div 
					class="card card-cascade narrower" 
					style="margin-top: -2px !important; width: 93% !important; margin-left: 20px !important; padding-top: 25px;
          padding-bottom: 25px;"
				>
				  <div class="px-4">
				    <div class="table-wrapper">
				      <!--Table-->
				      <table class="table table-hover mb-0" id="dtBasicExample">
				        <thead>
				          <tr>
				            <th>
				              <input 
				              	class="form-check-input" 
				              	type="checkbox" 
				              	id="checkbox"
				              	@click="selectAllCustom($event)" 
				              	v-model="allSelected"
				              >
				              <label 
				              	class="form-check-label" 
				              	for="checkbox" 
				              	class="mr-2 label-table">		
				              </label>
				            </th>
				            <th class="th-lg">
				              <a>Resgistro de areas selecionadas usuario
				                <i class="fas fa-sort ml-1"></i>
				              </a>
				            </th>
				          </tr>
				        </thead>
				        <tbody id="developers">
				          <tr 
				          	v-for="(data, index) in taskSelectUser">
				            <th scope="row">
				            	<!-- v-model="data.activo"  -->
				              <input 
				              	class="form-check-input" 
				              	type="checkbox" 
				              	id="checkbox1" 
				              	:checked="taskCheckAll[index]" 
				              	@click="addTaskCheck(taskSelectUser[index].id,$event)"
				              >
				              <label 
				              	class="form-check-label" 
				              	for="checkbox1" 
				              	class="label-table">
				              </label>
				            </th>
				            <td style="font-size: 11px;">{{ taskSelectUser[index].nom_tarea }}</td> 
				          </tr>
				        </tbody>
				      </table>
				      <table id="table-paginate" border="0" cellpadding="0" cellspacing="0" style="">
                <tbody>
                  <tr>  
                    <td class="gutter">
                      <div class="line number1 index0 alt2" style="display: none;">1</div>
                    </td>
                    <td class="code">
                      <div class="container" style="display: none;">
                        <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
				    </div>
				    
				  </div>
				</div>
      </div>
    </div>
  </div>
</div>