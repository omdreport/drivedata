<!DOCTYPE html>
<html>
  <head>
    <?php require_once './head.php'; ?>
  </head>
  <body>
      <?php require_once '../menu.php'?>
    <div class="row" style="width: 100% !important;" id="table-form">
      <div class="col-md-12">
        <section class="panel">
          <div class="panel-body">
            <div class="card" style="margin-top: 16px !important;">
              <div class="card-header text-center font-weight-bold py-4" style="height: 17px !important;"><!-- text-uppercase --> 
                <button type="button" class="btn btn-primary btn-add-btn" data-toggle="modal" data-target="#myModal" v-on:click="crear">Crear Tarea</button>
                <h3 class="title-table-list"><strong>Creacion de Tareas</strong></h3>
                <button type="button" class="btn btn-primary btn-add-btn" data-toggle="modal" data-target="#myModal2" v-on:click="reporte" style="margin-left: -32%;">Generar Reporte</button>
              </div>
              <div class="card-body">
                <div id="table">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th class="text-center" hidden="">Id</th>
                        <th class="text-center">Fecha</th>
                        <th class="text-center">Tarea</th>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Hora</th>
                        <th class="text-center">Descripcion</th>
                      </tr>
                    </thead>
                    <tbody id="tbody-render">
                      <tr>
							    			<td class='pt-3-half text-center' id='id-table' contenteditable='false' hidden></td> <!-- hidden -->
							    			<td class='pt-3-half text-center' contenteditable='false'></td>
							    			<td class='pt-3-half text-center' contenteditable='false'></td>
							    			<td class='pt-3-half text-center' contenteditable='false'></td>
							    			<td class='pt-3-half text-center' contenteditable='false'></td>
                        <td class='pt-3-half text-center' contenteditable='false'></td>
                      </tr>
                    </tbody>
                  </table> 
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td class="gutter">
                          <div class="line number1 index0 alt2" style="display: none;">1</div>
                        </td>
                        <td class="code">
                          <div class="container" style="display: none;">
                            <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
				          </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <!-- Modal Tareas -->
      <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header table-info">
              <h4 class="modal-title">Ingresar Tarea</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" id="form">
              <form>
                <table class="table table-hover">
                  <tr>
                      <td><label for="staticEmail" class="col-sm-2 col-form-label">Fecha</label></td>
                      <td>
                        <input type="date" class="form-control-plaintext" id="fecha" v-model="fecha" name='fecha' v-validate="'required'">
                        <span class="error" v-if="errors.has('fecha')">{{errors.first('fecha')}}</span>   
                      </td>
                  </tr>
                  <tr>
                      <td><label for="exampleSelect2" class="col-sm-2 col-form-label">Tarea</label></td>
                      <td>
                        <select multiple="" class="form-control" id="tarea" v-model="tarea" name="tarea" v-validate="'required'">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                        <span class="error" v-if="errors.has('tarea')">{{errors.first('tarea')}}</span>
                      </td>  
                  </tr>
                  <tr>
                      <td><label for="exampleSelect2" class="col-sm-2 col-form-label">Cliente</label></td>
                      <td>
                          <select  class="form-control"  @change="oncliente()" id="cliente" v-model="cliente" name="cliente" v-validate="'required'">
                            <option value="1">--Escoja Opcion---</option>
                          </select>
                        <span class="error" v-if="errors.has('cliente')">{{errors.first('cliente')}}</span>
                      </td>
                  </tr>
                  <tr>
                      <td><label for="staticEmail" class="col-sm-2 col-form-label">Hora</label></td>
                      <td><input type="number" class="form-control-plaintext" id="hora" v-model="hora" name="hora" v-validate="'required'">
                        <span class="error" v-if="errors.has('hora')">{{errors.first('hora')}}</span> 
                      </td>
                  </tr>
                  <tr>
                      <td><label for="desc" class="col-sm-2 col-form-label">Descripcion</label></td>
                      <td>
                        <textarea class="form-control" rows="3" v-model="area" name="area" v-validate="'required'"></textarea>
                        <span class="error" v-if="errors.has('area')">{{errors.first('area')}}</span> 
                      </td>
                  </tr>
                  <tr>
                      <td><button type="button" class="btn btn-success" v-on:click="agregar" >Agregar Tarea</button></td>
                  </tr>
                </table>
              </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer table-info">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>  
          </div>
        </div>
      </div>
      <!-- Modal Reporte -->
      <!-- The Modal -->
      <div class="modal fade" id="myModal2" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Reportes</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" id="form">
              <form>
                <table class="table table-hover">
                <tr>
                  <td><label>Fecha Inicial</label></td>
                  <td><input type="date" style="width: 350px !important; height: 30px;" id="fechai" v-model="fechai" name="fechai" v-validate="'required'">
                    <span class="error" v-if="errors.has('fechai')">{{errors.first('fechai')}}</span>  
                  </td> 
                </tr>
                <tr>
                  <td><label>Fecha Final</label></td>
                  <td><input type="date" style="width: 350px !important; height: 30px;" id="fechaf" v-model="fechaf" name="fechaf" v-validate="'required'">
                    <span class="error" v-if="errors.has('fechaf')">{{errors.first('fechaf')}}</span>
                  </td>    
                </tr>   
                <tr>
                  <td><label>Cliente<label></td>
                  <td>
                    <select  class="form-control"  @change="oncliente()" id="cliente2" v-model="cliente" name="cliente">
                      <option value="1">--Escoja Opcion---</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Generar Informe</td>
                  <td><button type="button" class="btn btn-primary" v-on:click="rep">Informe</button></td>
                </tr>
                </table>
              </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="../../public/js/vueService.js"></script>
  </body>
</html>