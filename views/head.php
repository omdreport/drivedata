<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>OMD</title>
    <!--libs css -->
    <link rel="stylesheet" href="../public/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../public/dataTable/css/dataTables.bootstrap4.min.css">
    <link rel="icon" type="image/png" href="../favicon-omd.png" sizes="32x32">
    <!--libs js -->
    <script src="../public/jquery/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../public/dataTable/js/jquery.dataTables.min.js"></script>
    <script src="../public/dataTable/js/dataTables.bootstrap4.min.js"></script> 
    <script src="../public/vue/vue.js"></script>
    <script src="https://unpkg.com/vee-validate@2.0.0-beta.25"></script>
    <script src="../public/libs/dist/sweetalert.min.js"></script>
    <!--<script src="../public/vue/dist/vee-validate.js"></script>-->
    <style>
      @import url('https://unpkg.com/semantic-ui-css@2.2.9/semantic.css');

      span.error {
        color: #9F3A38;
      }
      html,body{
        background-image: url('../public/img/dashboard.png');
        background-size: 100% 125%;;
        background-repeat: no-repeat;
        height: 100%;
      }
      .card{
        width: 86% !important;
        margin-left: 8% !important;
        margin-top: 52px !important;
      }
      .btn-add-user {
        width: 170px;
        height: 39px; 
        padding: 0pc; 
        font-size: 17px;
      }
      .modal-content-cumston {
        margin-top: 74px; 
        position: absolute;
      }
      #text-label {
        margin-left: -12px;
      }
      .btn-add-btn {
        height:24px; 
        width: 113px; 
        text-align:center;  
        margin-top: -12px;
        margin-left:-49%; 
        padding: 0px; 
        font-size: 14px; 
        position: absolute;
      }
      .btn-excel-btn {
        height:24px; 
        width: 158px; 
        text-align:center;   
        margin-top: -12px; 
        margin-left: -38%; 
        padding: 0px; 
        font-size: 14px; 
        position: absolute; 
        background-color: rgb(0, 177, 64);
      }
      .btn-excel-excel {
        height:24px; 
        width: 158px; 
        text-align:center;  
        margin-top: -12px; 
        margin-left: -23%; 
        padding: 0px; 
        font-size: 14px; 
        position: absolute; 
        background-color: rgb(0, 177, 64);
      }
      .title-table-list {
        font-size: 18px; 
        margin-top: -12px; 
        margin-left: 42%; 
        position: absolute;
      }
      @media (min-width: 1206px) {
        .btn-excel-btn{
          margin-left: -37% !important;
        }
        .btn-excel-excel{
          margin-left: -21% !important;
        }
        .title-table-list{
          margin-left: 45% !important;
        }
      }
      @media (min-width: 906px) {
        .btn-excel-btn{
          margin-left: -37% !important;
        }
        .btn-excel-excel{
          margin-left: -37% !important;
        }
        .title-table-list{
          margin-left: 27% !important;
        }
      }
      #table-paginate .dataTables_info{
        font-size: 11px !important;
      }
      .form-font {
        font-size: 14px !important;
      }
    </style>
</head>
<body>
    
</body>
</html>

