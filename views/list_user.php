<!DOCTYPE html>
<html>
  <head>
    <?php require_once './head.php' ?>
  </head>
  <body>
  	<!--Navbar -->
  	<?php require_once './menu.php'; ?>

    <div class="row" style="width: 100% !important;" id="app-table">
      <div class="col-md-12">
        <section class="panel"> 
          <div class="panel-body">
            <div class="card" style="margin-top: 16px !important;">
              <div class="card-header text-center font-weight-bold py-4" style="height: 17px !important;"><!-- text-uppercase -->
               
                <button type="button" class="btn btn-primary btn-add-btn" data-toggle="modal" data-target="#myModalUser" @click="addUser()">Crear Usuarios</button>
                <button type="button" class="btn btn-primary btn-excel-btn" @click="importCSV()" hidden>Reporte Usuarios csv</button>
                <button type="button" class="btn btn-primary btn-excel-excel" @click="importExcel()">Reporte Usuarios Excel</button>
                 <h3 class="title-table-list"><strong>Usuarios</strong></h3>
              </div>
              <div class="card-body">
                <div id="table">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    
                    <thead>
                      <tr>
                        <th class="text-center" hidden="">Id</th>
                        <th class="text-center">Nombres</th>
                        <th class="text-center">Apellidos</th>
                        <th class="text-center">usuario</th>
                        <th class="text-center">Perfil</th>
                        <th class="text-center">Perfil</th>
                        <th class="text-center">Cargo</th>
                        <th class="text-center">Activo</th>
                        <th class="text-center">Acciones</th>
                      </tr>
                    </thead>
                        
                    <tbody id="tbody-render">
                      <tr v-if="tableBody" v-for="(data, index) in tableData">
							    			<td class='pt-3-half text-center' id='id-table' contenteditable='false' hidden>{{ tableData[index].id }}</td> <!-- hidden -->
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_usuario }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].ape_usuario }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].usuario }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_role }}</td>
                        <td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_perfil }}</td>
                        <td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_perfil }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].activo }}</td>
							    			<td class='pt-3-half text-center'>
							    			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height:24px; width:67px; text-align:center; margin-top:-2px; padding: 0px; font-size: 14px;" @click="editUser(tableData[index])">Editar</button>
							    			</td>
                       </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td class="gutter">
                          <div class="line number1 index0 alt2" style="display: none;">1</div>
                        </td>
                        <td class="code">
                          <div class="container" style="display: none;">
                            <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- Editable table -->
          </div>
        </section>
      </div>
      <?php 
        include'layout/modal_user.php';
        include'layout/modal_add_user.php';
        include'layout/modal_task_user.php';
      ?>
    </div>
    <!--libs vuejs -->
    </div>
    <!--libs vuejs -->
    
   
   	<script src="../public/js/vueTableUsuario.js"></script>
  </body>
</html>