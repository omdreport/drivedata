//valida los formularios si los campos son requeridos
const config = {
	errorBagName: 'errors', // change if property conflicts
	 fieldsBagName: 'fields',
	 dictionary: null,
Locale: 'es',
}
Vue.use(VeeValidate, {locale: 'es'})
//Vue.use(VeeValidate, config);
Vue.config.devtools = true
//instancia de vuejs con el id de la pagina  de usuarios
var app3 = new Vue({
el: '#app-table',
locale: 'es',
data : {
	checked : true,
	message : '',
	error : false,
	tableBody : false,
	listTask : false,
	checked : false,
	//validar true checkbox 
	serchTask : false,
	serch : false,
	clientsCheckAll : [],
	taskCheckAll : [],
	//-----------
	tableData : [],
	dataPerfil : [],
	dataClients : [],
	dataRoles : [],
	dataActivo : [],
	taskSelectUser : [],
	taskAdd : [],
	clientsAdd : [],

	modalUser : false,
	dataEditUser : {
		id : null,
		nom_usuario : null,
		ape_usuario : null, 
		usuario : null,
		password : null,
		num_documento : null,
		id_role : null,
		id_perfil : null,
		activo : true,
		tarea : [],
		clients : [],
	},

	modalAddUser : false,
	dataAddUser : {
		nom_usuario : null,
		ape_usuario : null,
		usuario : null,
		password : null,
		num_documento : null,
		id_roles : null,
		id_perfil : null,
		tarea : [],
		clients : [],
	},
	allSelected: false,
	activo : true,
},

mounted : function (){
	this.getData()
	this.getAllData()
},

watch: {
	// whenever question changes, this function will run
	"dataAddUser.num_documentos" : function  (newDocument, oldDocument) {
		this.consultDocument(newDocument)
	}
},

methods : {
	/*** Obtener data de los usuarios ***/
	getData: function () {
		fetch('../../../app_omd/controllers/router.php?accion=getUsers', {
			method: 'GET',
			headers:{
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => {
			let status = response.status
			if(status == 200){
				this.tableBody = true
				this.tableData = response.data
				/** Inicializa la tabla de listar **/
				setTimeout(() => 
					$('#example').DataTable({
						language: {
							"emptyTable": "No hay información",
							"search": "Buscar:",
							"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
							"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
							"infoFiltered": "(Filtrado de _MAX_ total entradas)",
							"infoPostFix": "",
							"thousands": ",",
							"lengthMenu": "Mostrar _MENU_ Entradas",
							"loadingRecords": "Cargando...",
							"processing": "Procesando...",
							"zeroRecords": "Sin resultados encontrados",
							"paginate": {
								"first": "Primero",
								"last": "Ultimo",
								"next": "Siguiente",
								"previous": "Anterior"
							}
						},
					}), 300)
			}else if(status == 201){

			}else if(status == 500){ }
		})
	},

	consultDocument: function(documentoUsers){
		let documentoUser = documentoUsers.target.value
		fetch(`../../../app_omd/controllers/router.php?accion=validateDoc&documento=${documentoUser}`, {
			method: 'GET',
			headers:{
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => {
			let status = response.status
			if(status == 200){
				this.swallInfo('El numero de documento ya se encuentra registrado ','info')
			}else if(status == 201){
				//alert('el usurio no existe......')
			}else if(status == 500){ }
		})
	},
/** Clasificar Clientes mostrar en editar */
	editUser: function (dataUser) {
		this.clientsCheckAll = []
		this.dataEditUser = Object.assign({}, dataUser)
		const clientList = this.dataEditUser.id_cliente.split(",")
		for(let clients in this.dataClients){
			for(let clientsRegister in clientList){
				this.serch = (clientList[clientsRegister] == this.dataClients[clients].id) ? true : false
				if(this.serch){
					break 
				}
			}
			if(this.serch){
				this.clientsCheckAll.push(this.serch) 
				this.clientsAdd.push(this.dataClients[clients].id)
			}else{
				this.clientsCheckAll.push(this.serch)
			}
		}
		this.modalAddUser = false
		this.modalUser = true
	},
	/*** Editar  data de los usuarios ***/
	editUserForm: function(){
		if(this.taskAdd.length < 1){
			this.swallInfo('No ha seleccionado ninguna tarea para el usuario...','info')
			return
		}
		if(this.clientsAdd.length < 1){
			this.swallInfo('No ha seleccionado ningun Cliente para el usuario...','info')
			return
		}
		if(this.taskAdd.length >= 51){
			this.swallInfo('La cantidad maxima de tareas ha seleccionar son 50 tareas.','info')
			return
		}
		this.dataEditUser.clients = {"id_clients" : this.clientsAdd}
		this.dataEditUser.tarea   = {"id_tareas"  : this.taskAdd}
		this.$validator.validateAll().then(() => {
			fetch('../../../app_omd/controllers/router.php?accion=editUser', {
			  method: 'POST', 
			  body: JSON.stringify(this.dataEditUser), // data can be `string` or {object}!
			  headers:{
					'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(async  response => {
				let resSwall = ""
				let status = response.status
				if(status == 200){
			    resSwall = await this.swallInfo('Se actualizó correctamente la información...', 'success')
			    window.location.href = 'list_user.php'
				}else if(status == 201){
			    resSwall = await this.swallInfo('No hubo modificación de datos...', 'info')
			    window.location.href = 'list_user.php'
				}else if(status == 500){
			    resSwall = await this.swallInfo('No se Gurdo la información...', 'info')
				}
			})

	  }).catch(() => {
		  console.log(this.errors.all())
		  return false
	  })
	},

	addUser: function() {
		this.dataAddUser.id_perfil = this.dataPerfil[1].id
		this.dataAddUser.clients   = this.dataClients[1].id
		this.dataAddUser.id_roles  = this.dataRoles[1].id
		this.modalUser = false
		this.modalAddUser = true
	},

	/*** Agregar Usuarios ***/
	addUserForm: function() {
		//this.dataAddUser.
		if(this.taskAdd.length < 1){
			this.swallInfo('No ha seleccionado ninguna tarea para el usuario...','info')
			return
		}
		if(this.clientsAdd.length < 1){
			this.swallInfo('No ha seleccionado ningun Cliente para el usuario...','info')
			return
		}
		if(this.taskAdd.length >= 51){
			this.swallInfo('La cantidad maxima de tareas ha seleccionar son 50 tareas.','info')
		  return
		}
		// ********* AGREGAR INFO DEL FORMULARIO PARA INSERTAR
		// *********
		this.dataAddUser.clients = {"id_clients" : this.clientsAdd}
		this.dataAddUser.tarea   = {"id_tareas"  : this.taskAdd}
		this.$validator.validateAll().then(() => {
			fetch('../../../app_omd/controllers/router.php?accion=addUser', {
			  method: 'POST', 
			  body: JSON.stringify(this.dataAddUser), // data can be `string` or {object}!
			  headers:{
				'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(async  response => {
				let resSwall = ""
				let status = response.status
				if(status == 200){
					resSwall = await this.swallInfo('Se guardo correctamente la información...','success')
					window.location.href = 'list_user.php'
				}else if(status == 201){
					resSwall = await this.swallInfo('El usuario y la contraseña ya existen...', 'info')
					window.location.href = 'list_user.php'
				}else if(status == 204){
					resSwall = await this.swallInfo('El usuario y la contraseña ya existen...', 'info')
				}else if(status == 500){
					resSwall = await this.swallInfo('No se Gurdo la información...', 'info')
				}
			})

		}).catch(() => {
			return false
		})
	},
	/*** Obtener data de los usuarios para los select ***/
	getAllData: function() {
		fetch('../../../app_omd/controllers/router.php?accion=getAllData', {
		  method: 'GET',
		  headers:{
			'Content-Type': 'application/json'
		  }
		})
		.then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => {
			let status = response.status
			if(status == 200){
				this.dataActivo = [
			    { activo : 'Activo', id : 1 },
			    { activo : 'Inactivo', id : 0 }
				]
				this.dataPerfil  = response.perfil
				this.dataClients = response.clientes
				this.dataRoles   = response.roles
			}else if(status == 201){

			}else if(status == 500){ }
		})

	},
	getTaskSelect: function(event,checked){
		this.checked = event.target.checked
		if(this.checked===false){
			this.listTask = false
			return
		}
		if(this.checked===true){
			this.getDataTask()
			this.listTask = true
		}
	},
	getAreasSelect: function (event,checked){
	  this.checked = event.target.checked
		if(this.checked===false){
			this.listTask = false
			return
		}
		if(this.checked===true){
			this.listTask = true
			this.getDataTask()
		}
	},

	getDataTask: function (){
		fetch(`../../../app_omd/controllers/router.php?accion=getTask&taks=${this.dataAddUser.id_perfil}`, {
		  method: 'GET',
		  headers:{
			'Content-Type': 'application/json'
		  }
		})
		.then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(response => {
			let status = response.status
			if(status == 200){
				this.taskSelectUser = response.data
				setTimeout(() => {
					$('#dtBasicExample').DataTable({
				    language: {
					  	"search": "Buscar:",
					   	"emptyTable": "No hay información",
					   	"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
					   	"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
						  "infoFiltered": "(Filtrado de _MAX_ total entradas)",
						  "infoPostFix": "",
							"thousands": ",",
							"lengthMenu": "Mostrar _MENU_ Entradas",
							"loadingRecords": "Cargando...",
							"processing": "Procesando...",
							"zeroRecords": "Sin resultados encontrados",
							"paginate": {
								"first": "Primero",
								"last": "Ultimo",
								"next": "Siguiente",
								"previous": "Anterior"
		   				},
						},
						"lengthMenu": [[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
						"iDisplayLength":   10,
						//"paging": false // false to disable pagination (or any other option)
					})
					$('.dataTables_length').addClass('bs-select')
					this.checkListTask()
				},300)
			}else if(status == 201){

			}else if(status == 500){
							
			}
		})
	},

	checkListTask: function(){
		this.taskCheckAll = []
		const jsonTask = JSON.parse(this.dataEditUser.tareas)
			for(let task in this.taskSelectUser){
				for(let dataTask in jsonTask.id_tareas){
					this.serchTask = (jsonTask.id_tareas[dataTask] == this.taskSelectUser[task].id) ? true : false
					if(this.serchTask){
						break 
					}
				}
				if(this.serchTask){
					this.taskCheckAll.push(this.serchTask) 
					this.taskAdd.push(this.taskSelectUser[task].id)
				}else{
					this.taskCheckAll.push(this.serchTask)
				}
			}
		 //console.log(this.taskCheckAll)
	},

	addCheckTask: function(){
		swal({
		  title: "Estas seguro de agregar las tareas.",
		  icon: "info",
		  buttons: true,
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Aceptar",
		  cancelButtonText: "Cancelar",
		 closeOnConfirm: false,
		  closeOnCancel: false
		})
		.then((isConfirm) => {
		  if (isConfirm) {
		   this.listTask = false
		  }
		})
	},

	addTaskCheck: function(idTask, event){
		this.allSelected = false
		let check = event.target.checked
		if( check===true ){
			this.taskAdd.push(idTask)
		}
		else if( check===false ){
			if( this.taskAdd.length > 0 ){
		   let cat = this.taskAdd.filter((i)=>{return i == idTask})
		   this.taskAdd = this.taskAdd.filter((i)=>{return i != idTask}).map((e)=>{return e})
							
			}else if( this.taskAdd.length == 0 ){
				this.taskAdd = this.taskAdd.filter((i)=>{return this.taskAdd!=idTask}).map((e)=>{return e})
				this.taskAdd = []
			}
		}
	},
	/** Agrega a la lista los clientes al editar **/
	selectAllCustom: function(event) {
		this.allSelected = true
		let eventCheck = event.target.checked
		this.taskAdd = []
		for(task in this.taskSelectUser) {
			this.taskSelectUser[task].activo = false
		}
		if (eventCheck) {
			let activo = true
			for (task in this.taskSelectUser) {
				this.taskSelectUser[task].activo = true
				this.taskAdd.push(this.taskSelectUser[task].id)
			}
		}
},
/** Agrega a la lista los clientes seleccionados **/
getClientsSelect(event,indexClient){
	let checkClient = event.target.checked
	if(checkClient){
		this.clientsAdd.push(indexClient.id)       
	}else if(checkClient == false){
		let clientDelete = this.clientsAdd.filter((e,i)=>{return e != indexClient.id})
		this.clientsAdd = clientDelete
	}
},

	importExcel: function    (){
		window.location="../../../app_omd/controllers/routerExcel.php?accion=importDataxls"
	},

	swallInfo: async function(title, icon){
	 await    
	  swal({
		  title: title,
		  icon: icon,
		  button: "Aceptar",
		})
	 return true
	}
},

created () {
	
}
})
