<?php 
require_once 'connexion.php';
require_once '../Classes/PHPExcel.php';
require_once '../Classes/PHPExcel/Writer/Excel2007.php';

class ImportExcel {
	private $sqlExcel;
	private $queryExcel;
	private $resExcel;
	private $objPHPExcel;
	private $fi;
	private $ff;
	private $iclient;


	function __construct (){
		$this->connect = new connexion();
		$this->connect->getConnection();
		// Crea un nuevo objeto PHPExcel
		$this->objPHPExcel = new PHPExcel();
		# code...
	}

	public function importCsvUser (){
		try {
			// Establecer propiedades
			$this->resExcel = array();
			$this->sqlExcel = "SELECT u.nom_usuario, u.ape_usuario, u.usuario, u.num_documento, p.nom_perfil, a.nom_area 
												 FROM usuario u 
												 INNER JOIN areas a ON u.id_area = a.id 
												 INNER	 JOIN perfil p ON u.id_perfil = p.id ";
			$this->queryExcel = $this->connect->connect->prepare($this->sqlExcel); 
			$this->queryExcel->execute();
			$this->resExcel	= $this->queryExcel->fetchAll(PDO::FETCH_CLASS);;	
			$i = 2;
			$file = fopen('php://output', 'w');
			$columsHeaders = array('nombres', 'apellidos', 'usuario', 'documento', 'perfil', 'area');
			$delimiter =',';
			fputcsv($file, $columsHeaders, $delimiter);

			foreach ($this->resExcel as $key => $value) {
				$aux = $i + $key;
				$lineData = array(
					$this->resExcel[$key]->nom_usuario,
					$this->resExcel[$key]->ape_usuario,
					$this->resExcel[$key]->usuario,
					$this->resExcel[$key]->num_documento,
					$this->resExcel[$key]->nom_perfil,
					$this->resExcel[$key]->nom_area
				);
				fputcsv($file, $lineData, $delimiter);
			}
			//move back to beginning of file
    	//fseek($file, 0);
			header('Content-Type: text/csv; charset=utf-8');  
			//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reporteExcel.csv"');
			header('Cache-Control: max-age=0');
			fpassthru($file);
			fclose($file);
		} catch (Exception $e) {
			
		}
	}

	public function importExcelUser (){
		try {
			// Establecer propiedades
			$this->resExcel = array();
			$filename = "reporte_usuario.xls";
			$this->objPHPExcel->getProperties()
				->setTitle("Documento Excel")
				->setKeywords("Excel Office 2007 openxml php")
				->setCategory("Reporte de Excel");

			// Agregar Informacion
			$this->sqlExcel = "SELECT u.nom_usuario, u.ape_usuario, u.usuario, u.num_documento, p.nom_perfil, a.nom_area 
												 FROM usuario u 
												 INNER JOIN areas a ON u.id_area = a.id 
												 INNER	 JOIN perfil p ON u.id_perfil = p.id ";
			$this->queryExcel = $this->connect->connect->prepare($this->sqlExcel); 
			$this->queryExcel->execute();
			$this->resExcel	= $this->queryExcel->fetchAll(PDO::FETCH_CLASS);

			$this->objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'nombres')
				->setCellValue('B1', 'apellidos')
				->setCellValue('C1', 'usuario')
				->setCellValue('D1', 'documento')
				->setCellValue('E1', 'perfil')
				->setCellValue('F1', 'area');

			// Fuente de la primera fila en negrita
			$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY));
			$this->objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($boldArray);

				//Ancho de las columnas
				$this->objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);	
				$this->objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);	
				$this->objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);	
				$this->objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);	
				$this->objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$this->objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);	
				$i = 2;
				$aux=0;
			foreach ($this->resExcel as $key => $value) {
				$aux = $i + $key;
				# code...
				$this->objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$aux, $this->resExcel[$key]->nom_usuario)
					->setCellValue('B'.$aux, $this->resExcel[$key]->ape_usuario)
					->setCellValue('C'.$aux, $this->resExcel[$key]->usuario)
					->setCellValue('D'.$aux, $this->resExcel[$key]->num_documento)
					->setCellValue('E'.$aux, $this->resExcel[$key]->nom_perfil)
					->setCellValue('F'.$aux, $this->resExcel[$key]->nom_area);

			}
			$aux = ($aux==0) ? 2 : $aux;
			$boldArray = array('font' => array('bold' => false,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY));
			$this->objPHPExcel->getActiveSheet()->getStyle('A2:F'.$aux)->applyFromArray($boldArray);
			// Renombrar Hoja
			$this->objPHPExcel->getActiveSheet()->setTitle('Reporte Usuarios');
			// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
			$this->objPHPExcel->setActiveSheetIndex(0);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reporte_usuario.xls"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		  $objWriter->save($filename);
		  readfile($filename);
				
		} catch (Exception $e) {
			
		}
	}

	public function importExcelReport($fechai, $fechaf, $idcliente){
		try {
			// Establecer propiedades
			$this->fi = $fechai;
			$this->ff = $fechaf;
			$this->iclient = $idcliente;
			$this->resExcel = array();
			$filename = "reporte_tarea.xls";
			$this->objPHPExcel->getProperties()
				->setTitle("Documento Excel")
				->setKeywords("Excel Office 2007 openxml php")
				->setCategory("Reporte de Excel");
			
			// Agregar Informacion
			$this->sqlExcel = "SELECT s.fecha_creacion, s.nom_servicio, s.hora, s.descripcion, c.nom_cliente, u.nom_usuario
			 					FROM servicio s 
								INNER JOIN cliente c on s.id_cliente = c.id
								INNER JOIN usuario u on s.id_usuario = u.id 
								WHERE s.fecha_creacion BETWEEN '$this->fi' AND '$this->ff' AND id_cliente= 1";
			$this->queryExcel = $this->connect->connect->prepare($this->sqlExcel);
			$this->queryExcel->execute();
			$this->resExcel = $this->queryExcel->fetchAll(PDO::FETCH_CLASS);
			//print_r($this->resExcel);
			$this->objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1','fecha')
				->setCellValue('B1','tarea')
				->setCellValue('C1','hora')
				->setCellValue('D1','descripcion')
				->setCellValue('E1','cliente')
				->setCellValue('F1','usuario');
			
			// Fuente de la primera fila en negrita
			$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY));
			$this->objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($boldArray);

			//Ancho de las columnas
			$this->objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);	
			$this->objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);	
			$this->objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);	
			$this->objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);	
			$this->objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
			$this->objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			$i=2;
			foreach($this->resExcel as $key => $value){
				$aux = $i + $key;
				$this->objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$aux, $this->resExcel[$key]->fecha_creacion)
				->setCellValue('B'.$aux,$this->resExcel[$key]->nom_servicio)
				->setCellValue('C'.$aux,$this->resExcel[$key]->hora)
				->setCellValue('D'.$aux,$this->resExcel[$key]->descripcion)
				->setCellValue('E'.$aux,$this->resExcel[$key]->nom_cliente)
				->setCellValue('F'.$aux,$this->resExcel[$key]->nom_usuario);
			}
			$boldArray = array('font' => array('bold' => false,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY));
			$this->objPHPExcel->getActiveSheet()->getStyle('A2:F'.$aux)->applyFromArray($boldArray);
			// Renombrar Hoja
			$this->objPHPExcel->getActiveSheet()->setTitle('Reporte Usuarios');
			// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
			$this->objPHPExcel->setActiveSheetIndex(0);
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reporte_usuario.xls"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		  	$objWriter->save($filename);
		  	readfile($filename);

		} catch (PDOException $e) {
			
		}
	}


}