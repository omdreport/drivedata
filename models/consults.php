<?php 
require_once 'connexion.php';

/**
 * 
 */
class Consults
{
	private $sql;
	private $query;
	private $sql2;
	private $query2;
	private $sqlValidate;
	private $queryValidate;
	private $res;
	private $sql3;
	private $query3;
	
	function __construct()
	{
		$this->connect = new connexion();
		$this->connect->getConnection();
		# code...
	}

	public function getUsers() {

		try {
			$this->res= array();
			$this->sql = "SELECT 
											u.id AS id, 
											u.num_documento,
											u.nom_usuario AS nom_usuario, 
											u.ape_usuario AS ape_usuario, u.usuario AS usuario, 
											u.tareas,
											u.contrasena as password,
											CASE WHEN  u.activo = 1 THEN 'Activo' ELSE 'Inactivo' END AS activo,
											u.activo AS activ,
											p.nom_perfil AS nom_perfil,
											p.id as id_perfil,
											r.nom_role AS nom_role,
											r.id as id_role,
											JSON_ARRAYAGG(cl.id) as json_id_cliente,
											GROUP_CONCAT(DISTINCT cl.id) AS id_cliente --  ORDER BY cl.id  DISTINCT
										FROM usuario AS u
										INNER JOIN perfil p ON u.id_perfil = p.id
										INNER JOIN roles r ON p.id_role = r.id
										INNER JOIN usuario_cliente uc ON u.id = uc.id_usuario		
										INNER JOIN cliente cl ON uc.id_cliente = cl.id
										GROUP BY id";
			$this->query = $this->connect->connect->query($this->sql);
			$rows = $this->query->fetchAll(PDO::FETCH_CLASS);
			if($this->query->rowCount() > 0) {	
				$this->res['data'] = $rows;
				$this->res['status'] = 200;
			} else {
				$this->res['status'] = 201;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		}finally {
			return $this->res;
		}
	}

	public function getAllData() {

		try {
			$this->res = array();
			$this->sql = "SELECT id, nom_perfil FROM perfil";
			$this->query = $this->connect->connect->query($this->sql);

			if($this->query->rowCount() > 0) {
				$rowsPerfil = $this->query->fetchAll(PDO::FETCH_CLASS);
				/*
				obtener data de los 
				SELECT * FROM cliente ORDER BY nom_cliente ASC;
				 */
				$this->sql2 = "	SELECT * FROM cliente ORDER BY id ASC;";
				$this->query2 = $this->connect->connect->query($this->sql2);
			}

			if($this->query2->rowCount() > 0){
				$rowsArea = $this->query2->fetchAll(PDO::FETCH_CLASS);
				$this->res['perfil'] = $rowsPerfil;
				$this->res['areas']  = $rowsArea;
				$this->res['status'] = 200;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		} finally {
			return $this->res;
		}

	}


	public function  getTaskPeril($getRequest) {

		try {
			$this->res = array();
			$this->sql = "SELECT id, nom_tarea, null as activo FROM tareas limit 200";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if( $this->query->rowCount() > 0){
				$rowsPerfil = $this->query->fetchAll(PDO::FETCH_CLASS);

				$this->res['data'] = $rowsPerfil;
				$this->res['status'] = 200;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		}finally {
			return $this->res;
		}

	}

	public function authUsers(Request $request){

		try {
			$this->res= array();
			$user = $request->request['userName'];
			$pass = $request->request['userPass'];
			$this->sql ="SELECT * 
									 FROM usuario 
									 WHERE usuario = '$user' 
									 AND contrasena = '$pass'
									 AND activo = 1";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if($this->query->rowCount() > 0 ){
				$data = $this->query->fetchAll();
				$this->res['data'] = $data;
				$this->res['status'] = 200;
			}else{
				$this->res['status'] = 201;
			}
		} catch (PDOException  $e) {
			$this->res['status'] = 500;
			//echo $e->getMessage();
		} finally {
			return $this->res;
		}
		
	}

	public function updateUsers(Request $request){

		try {

			$idUsuario    = $request->request['id'];
			$nomUsuario   = $request->request['nom_usuario'];
			$apeUsuario   = $request->request['ape_usuario'];
			$numDocumento = $request->request['num_documento'];
			$usuario      = $request->request['usuario'];
			$idPerfil     = $request->request['id_perfil'];
			$idClients    = $request->request['clients']['id_clients'];
			$idTareas		  = json_encode($request->request['tarea']);
			//$activo      = (($request->request['activ'] == 0 ) ? (string)$request->request['activ'] : $request->request['activ']);
			$this->res   = array();
			$this->sql = "UPDATE usuario SET 
											nom_usuario   = '$nomUsuario', 
											ape_usuario   = '$ape_usuario', 
											num_documento = '$numDocumento'
											usuario       = '$usuario',
											id_perfil     = $idPerfil,
											tareas        = '$idTareas'
										WHERE id        = $idUsuario ";

			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if($this->query->rowCount() > 0 ){
				$this->res['status'] = 200;
			}else{
				$this->res['status'] = 201;
			}
		} catch (PDOException $e) {
			$this->res['status'] = 500;
		} finally {
			return $this->res;
		}
	}

	public function createUser(Request $request){

		try {
			$this->res = array();
			$resValidate =  self::validateUsers($request);
			if($resValidate == 2 ){
				$nomUsuario    = $request->request['nom_usuario'];
				$apeUsuario    = $request->request['ape_usuario'];
				$usuario       = $request->request['usuario']; 
				$password      = $request->request['password'];
				$numDocumento  = $request->request['num_documento'];
				$idPerfil      = $request->request['id_perfil'];
				$idClients     = $request->request['clients']['id_clients'];
				$tipoDocumento = 'Cedula';
				$idCiudad 		 = 1;
				$idTareas		   = json_encode($request->request['tarea']);
				$this->sql="INSERT INTO 
												usuario (
													nom_usuario,
													ape_usuario,
													usuario,
													contrasena,
													id_ciudad,
													id_perfil,
													tipo_documento,
													num_documento,
													tareas)
											VALUES 
												('$nomUsuario',
												 '$apeUsuario',
												 '$usuario',
												 '$password',
												 	$idCiudad, 
												 	$idPerfil,
												 '$tipoDocumento',
												 '$numDocumento',
												 '$idTareas')";
				$this->query = $this->connect->connect->prepare($this->sql);
				$this->query->execute();
				$this->sql3 = "SELECT id,num_documento FROM usuario WHERE num_documento='$numDocumento'";
				$this->query3  = $this->connect->connect->prepare($this->sql3);
				$this->query3->execute();
				if($this->query3->rowCount() > 0){
					$arrUsuario=$this->query3->fetchAll(PDO::FETCH_ASSOC);
					$idUsuario = $arrUsuario[0]['id'];
					$count = 0;
					$insertClient = "";
					for($i = 1; $i <= count($idClients); $i++){
						$insertClient .= ($i == count($idClients)) ? "($idUsuario,$idClients[$count])" : "($idUsuario,$idClients[$count]),";
						$count++;
					} 
					$this->sql2 = "INSERT INTO usuario_cliente (id_usuario,id_cliente) VALUES ".$insertClient.";";
					$this->query2 = $this->connect->connect->prepare($this->sql2);
					$this->query2->execute();
					if($this->query2->rowCount() > 0){
						$this->res['status'] = 200;
						$this->res['message'] = 'ok';
					}else{
						$this->res['status'] = 202;
						$this->res['message'] = 'error';
					}
				}else{
					$this->res['status'] = 201;
					$this->res['message'] = 'ok';
				}
			}else{
				$this->res['status'] = 204;
				$this->res['message'] = 'existe';
			}
			
		} catch (PDOException $e) {
			$this->res['status'] = 500;
			$this->res['message'] = 'error';
		} finally{
			return $this->res;
		}
		
	}
 /** 
  * Validar Si e l usuario ya existe *
  * @param  [type] $request [description]
  * @return [type]          [description]
  */
	private function validateUsers($request){

		try {
			$resValidate;
			$usuario  = $request->request['usuario']; 
			$password = $request->request['password'];
			$this->sqlValidate = "SELECT id, usuario FROM usuario WHERE usuario = '$usuario' AND contrasena = '$password'";
			$this->queryValidate = $this->connect->connect->prepare($this->sqlValidate);
			$this->queryValidate->execute();
			if($this->queryValidate->rowCount() > 0 ){
				$resValidate = 1;
			}else{
				$resValidate = 2;
			}
		} catch (Exception $e) {
			 $resValidate = 3;
		}finally {
			return $resValidate;
		}

	}

	public function Obtenercliente(){
		$this->sql = "SELECT * from cliente";
		$queryValidate = $this->connect->connect->query($this->sql);
		$datos = $queryValidate->fetchAll();
		//print_r($datos);
		return $datos;
		//die();
	}

	public function getResponseDocument($document){
		try {
			$this->res = array();
			$this->sql = "SELECT id, nom_usuario FROM usuario WHERE num_documento='$document'";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
	 		if($this->query->rowCount() > 0){
	 			$this->res['status'] = 200;
	 		}else{
	 			$this->res['status'] = 201;
	 		}
		} catch (Exception $e) {
			$this->res['status'] = 500;
		}finally {
			return $this->res;
		}
	}
}