# $ {1: omdata}
TODO: nombre del proyecto 
## Instalación
TODO: 
requiere php version 5.6 
requiere mysql version 5.6 
require bootstrap version 4.3.1
requiere jquey  version 3.3.1
requiere ajax version 1.14.7
requiere dataTables version  1.10.20
require vee-validate version 2.0.0-beta.25
require vue cdn version 2.6.10
requiere php excel libreria 
require xampp version php 5.6 en adelante

## arquitectura
TODO: arquitecturaq mvc 

## directorios proyecto 
TODO: 
Classes: se encuentra la libreria PHPExcel que nos permite descargar archivos .csv .xls .xlsx que se utilizan en importExcel.php

controllers: comunicacion con la carpeta views y models procesa peticiones HTTP  GET POST PUT DELETE

models: en models se encuentra la logica DML que procesa la información que llega del directorio controllers y retorna al mismo 
controlador la información  obtenida por medio de un array que devuelve 
['status'] ['data']  el status puede ser 200 = ok  201 = no hay información 500 = error de sintaxis, data es igual a la informacion devuelta por la base de datos
	dentro de models: encontramos la clase Request que es la encargada de recorrer la data que llega por el metodo POST PUT 
	dentro de models: encontramos la clase connexion que es la encargada de connectar nuestra base de datos por medio de php utilizando 
		PDO creando instancia de esta clase  utilizando  la funcion getConnection() y su variable connect, PDO::  ejemplo mas adelante.

public: se encuentra los siguientes directorios
	cdn de bootstrap en el directorio ./bootstrap clases que utilizamos en nuestras plantillas para simplificar el diseño
	css son estilos creados por los desarrolladores de la aplicación ./css si necesitamos personalizar estilos tanto de bootstrap como de nosotros
	cdn dataTable en el directorio ./dataTable se utiliza para crear las tablas, paginación, busqueda entre otras utilidades
	img dentro de este directorio se encuentran las imagenes que utilizaremos en nuestra app ./img
	cdn jquery en el directorio ./jquery se utiliza como complemento de dataTable 
	js se ecuentra la logica de las views validaciones de formularios, carga de data para las tablas el directorio ./js utiliza vuejs siempre se hace una unica instancia por template que se encarga de enrutar al directorio ./controllers

libs dentro del directorio ./libs se cargan librerias que devuelven errores o validaciones

vue dentro del directorio ./vue se carga el cdn de vuejs para crear instancias en el directorio de ./js en cada archivo que vallamos creando
	dist este directorio se utilizan los complementos para vuejs ./dist

views dentro del directorio ./views se encuentra la vista o template que crearemos los desarrolladores con forme los mudulos que requiera la aplicación
	dentro de views creamos subdirectorios que tendran vistas o template que requira un modulo grande y podamos minimalizar dicho modulo

el archivo index.php es el archivo principal de nuestro proyecto es que nos da el acceso al aplicativo a los usuarios registrados 

favicon-omd.png es el icono que se muestra como logo en cada pagina que se carga 

## manual PDO::
TODO: 
funciones basicas 
query() query se ejecuta cuando creamos un DML tipo select sin where va acompañado de la función fetchAll() crea un array con toda la 
	información devuelta
prepare() se ejecuta cunado creamos un DML tipo select con la combinación where, insert, update, delete´va acompañada de la funcion execute()

rowCount() valida si el lenguaje DML que se ejecuto afecto una fila o no devuelve 0 mayor que 0 
